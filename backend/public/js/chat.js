const socket = io('http://localhost:3000')

socket.on('connect', function () {
    console.log('Connected')
})

const messageInput = document.getElementById('messageInput')
messageInput.addEventListener('keyup', handleMessageChange)

function handleMessageChange(e) {
    socket.emit('typing', 'typing ...')
}

console.log('loaded')