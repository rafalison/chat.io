const express = require('express')
const app = express()
const http = require('http').createServer(app)
const io = require('socket.io')(http)

app.use(express.json())
app.use('/static', express.static('public'))
app.set('view engine', 'pug')
app.set('views', './views')

app.get('/', function(req, res) {
    res.render('chat', {
        messages: ['Hello', 'How are you?']
    })
})

io.on('connection', function(socket){
    console.log('user connected');
    socket.on('typing', (message) => console.log(message))
  });

http.listen(3000, function() {
    console.log('Server running')
})